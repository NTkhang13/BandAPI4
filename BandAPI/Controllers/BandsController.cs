﻿using AutoMapper;
using BandAPI.Business.Services;
using BandAPI.Entities;

using Inmergers.Business.Services;
using Inmergers.Common.Until;
using Inmergers.Common.Until.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace BandAPI.Controllers
{
    [ApiController]
    [Route("api/bands")]
    public class BandsController : ControllerBase
    {
        private readonly IBandAlbumRepository _bandAlbumRepository;
        private readonly IMapper _mapper;
        //private readonly IPropertyMappingService _propertyMappingService;
        //private readonly IPropertyValidationService _propertyValidationService;
        public BandsController(IBandAlbumRepository bandAlbumRepository, IMapper mapper/*, IPropertyMappingService propertyMappingService, IPropertyValidationService propertyValidationService*/)
        {

            _bandAlbumRepository = bandAlbumRepository ??
                throw new ArgumentNullException(nameof(bandAlbumRepository));
            _mapper = mapper ??
                throw new ArgumentNullException(nameof(mapper));
        }
        //https://localhost:44366/api/bands?fields=id,name

        #region CRUD
        [HttpGet(Name = "GetBands")]
        [HttpHead]
        public async Task <IActionResult> Get([FromQuery] BandQueryModel bandsResourceParameters)
        {

            var bandsFromRepo = await _bandAlbumRepository.Get(bandsResourceParameters);

            return Helper.TransformData(bandsFromRepo);

        }
       

        [HttpGet("{bandId}", Name = "GetBand")]
        public async Task  <IActionResult> GetById(Guid bandId/*, string fields*/)
        {
            /*//var bandFromRepo = _bandAlbumRepository.GetBand(bandId);

            //if (bandFromRepo == null)
            //    return NotFound();

            //return Ok(_mapper.Map<BandDto>(bandFromRepo).ShapeDate(fields));*/

            // Call service
            var result = await _bandAlbumRepository.GetById(bandId);
            // Hander response
            return Helper.TransformData(result);
        }

        [HttpPost]
        public async Task <IActionResult> CreateBand([FromBody] BandCreateModel band)
        {
          /*  //var bandEntity = _mapper.Map<Entities.Band>(band);
            //_bandAlbumRepository.AddBand(bandEntity);
            //_bandAlbumRepository.Save();

            //var bandToReturn = _mapper.Map<BandDto>(bandEntity);

            //return CreatedAtRoute("GetBand", new { bandId = bandToReturn.Id }, bandToReturn);

            // Get Token Info
            //var requestInfo = Helper.GetRequestInfo(Request);
            //var actorId = requestInfo.UserId;*/


            // Call service
            var result = await _bandAlbumRepository.Create(band);
            // Hander response
            return Helper.TransformData(result);

        }
        [HttpPut]
        public async Task<IActionResult> UpdateBand(Guid id, [FromBody] BandUpdateModel band)
        {
            var result = await _bandAlbumRepository.Update(id, band);

            return Helper.TransformData(result);
        }


        [HttpOptions]
        public IActionResult GetBandsOptions()
        {
            Response.Headers.Add("Alow", "GET,POST,DELETE,HEAD,OPTIONS");
            return Ok();
        }
        [HttpDelete("{bandId}")]
        public async Task <IActionResult> DeleteBand(Guid bandId)
        {
            var result = await _bandAlbumRepository.DeleteBand(bandId);
            return Helper.TransformData(result);
        }
        private string CreateBandsUri(BandsResourceParameters bandsResourceParameters, UriType uriType)
        {
            switch (uriType)
            {
                case UriType.PreviousPage:
                    return Url.Link("GetBands", new
                    {
                        fields = bandsResourceParameters.Fields,
                        orderBy = bandsResourceParameters.OrderBy,
                        pageNumber = bandsResourceParameters.PageNumber - 1,
                        pageSize = bandsResourceParameters.PageSize,
                        mainGenre = bandsResourceParameters.MainGenre,
                        searchQuery = bandsResourceParameters.SearchQuery
                    });
                case UriType.NextPage:
                    return Url.Link("GetBands", new
                    {
                        fields = bandsResourceParameters.Fields,
                        orderBy = bandsResourceParameters.OrderBy,
                        pageNumber = bandsResourceParameters.PageNumber + 1,
                        pageSize = bandsResourceParameters.PageSize,
                        mainGenre = bandsResourceParameters.MainGenre,
                        searchQuery = bandsResourceParameters.SearchQuery
                    });
                default:
                    return Url.Link("GetBands", new
                    {
                        fields = bandsResourceParameters.Fields,
                        orderBy = bandsResourceParameters.OrderBy,
                        pageNumber = bandsResourceParameters.PageNumber,
                        pageSize = bandsResourceParameters.PageSize,
                        mainGenre = bandsResourceParameters.MainGenre,
                        searchQuery = bandsResourceParameters.SearchQuery
                    });
            }
        }
    }
    #endregion CRUD
}
