﻿using AutoMapper;
using BandAPI.Models;
using BandAPI;

namespace BandAPI.Profiles
{
    public class AlbumsProfile : Profile
    {
        public AlbumsProfile() 
        {
            CreateMap<Entities.Album, Models.AlbumsDto>().ReverseMap();
            CreateMap<AlbumForCreatingDto, Entities.Album>();
            CreateMap<Models.AlbumForUpdatingDto, Entities.Album>().ReverseMap();
        }
    }
}
