﻿using BandAPI.Entities;
using Inmergers.Common.Until;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inmergers.Business.Services
{
    public class AlbumModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Band Band { get; set; }
        public Guid BandId { get; set; }
    }
    public class AddAlbumModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
    public class UpdateAlbumModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }
    public class AlbumQueryModel : PaginationRequest
    {
        public Guid? Id { get; set; }
    }
}
