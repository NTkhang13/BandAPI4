﻿using AutoMapper;
using AutoMapper.Configuration;
using BandApi.Data;
using BandAPI.Business.Services;
using BandAPI.DbContexts;
using BandAPI.Entities;
using BandAPI.Models;
using Inmergers.Business.Services;
using Inmergers.Common;
using Inmergers.Common.Until;
using Inmergers.Common.Until.Helpers;
using Inmergers.Data;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Stripe;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using Code = System.Net.HttpStatusCode;


namespace BandAPI.Services
{
    public class BandAlbumRepository : IBandAlbumRepository
    {
        private readonly BandAlbumContext _context;
        private readonly IMapper _mapper;

        //private readonly IPropertyMappingService _propertyMappingService;

        public BandAlbumRepository(BandAlbumContext context, IMapper mapper /*IUnitOfWork unitOfWork*//*, IPropertyMappingService propertyMappingService*/)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            //_propertyMappingService = propertyMappingService;
        }

        public async Task<Response> Get(BandQueryModel filter)
        {
            try
            {
                var predicate = BuildQuery(filter);
                var result = _context.Bands.Where(predicate).GetPage(filter);
                return new ResponsePagination<Band> (result);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        private Expression<Func<Band, bool>> BuildQuery(BandQueryModel query)
                {
                    var predicate = PredicateBuilder.New<Band>(true);

                    if (query.Id.HasValue && query.Id.Value != Guid.Empty)
                    {
                        predicate = predicate.And(s => s.Id == query.Id);
                    }

                    if (!string.IsNullOrEmpty(query.FullTextSearch))
                        predicate.And(s => s.Name.Contains(query.FullTextSearch)
                        || s.MainGener.Contains(query.FullTextSearch));

                    return predicate;
                }

        public async Task<Response> GetById(Guid BandId)
        {
            try
            {
               
                var entity = await _context.Bands.Where(x => x.Id == BandId).FirstOrDefaultAsync();

                if (entity == null)
                    return new ResponseError(Code.NotFound, "Không tìm thấy bản ghi");

                var data = _mapper.Map<Band,BandDto>(entity);
                return new ResponseObject<BandDto>(data);
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }


        public async Task<Response> Create(BandCreateModel filter)
        {
            try
            {
                var entityModel = new Band
                {
                    Name = filter.Name,
                    Founded = filter.Founded,
                    MainGener = filter.MainGener


                };

                _context.Add(entityModel);

                var status = await _context.SaveChangesAsync();

                if (status > 0)
                {
                    var data = _mapper.Map<Band, BandDto>(entityModel);
                    return new ResponseObject<BandDto>(data, "Thêm mới thành công");
                    

                }

                return new ResponseError(Code.NotFound, "Thêm mới thất bại");
            }
            catch (Exception ex)
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }

        public async Task<Response> Update(Guid id, BandUpdateModel model)
        {
            try
            {
                
                var entityModel = await _context.Bands.Where(x => x.Id == id).FirstOrDefaultAsync();

                if (entityModel == null)
                    return new ResponseError(Code.NotFound, "Không tìm thấy bản ghi");

                entityModel.Name = model.Name;
                entityModel.Founded = model.Founded;
                entityModel.MainGener = model.MainGener;


                var status = await _context.SaveChangesAsync();

                if (status > 0)
                {
                    var data = _mapper.Map<Band, BandDto>(entityModel);
                    return new ResponseObject<BandDto>(data, "Cập nhật thành công");
                    //return new Response(Code.OK, "Thêm mới thành công");

                }

                return new ResponseError(Code.NotFound, "Cập nhật thất bại");
            }
            catch (Exception ex)   
            {
                Log.Error(ex, string.Empty);
                return new ResponseError(Code.InternalServerError, ex.Message);
            }
        }


        public async Task<Response> DeleteBand(Guid bandId)
        {
            var bandFromRepo = _context.Bands.Where(x => x.Id == bandId).FirstOrDefault();
            if (bandFromRepo == null)
                return new Response(Code.OK, "Không tồn tại");

            _context.Bands.Remove(bandFromRepo);
            var status = await _context.SaveChangesAsync();
            return new Response(Code.OK, "Xóa thành công");
        }

        public void AddAlbum(Guid bandId, Album album)
        {
            if (bandId == Guid.Empty)
                throw new ArgumentNullException(nameof(bandId));

            if (album == null)
                throw new ArgumentNullException(nameof(album));

            album.BandId = bandId;
            _context.Albums.Add(album);
        }

        public void AddBand(Band band)
        {
            if (band == null)
                throw new ArgumentNullException(nameof(band));
            _context.Bands.Add(band);
        }

        public bool AlbumExists(Guid albumId)
        {
            if (albumId == Guid.Empty)
                throw new ArgumentNullException(nameof(albumId));

            return _context.Albums.Any(a => a.Id == albumId);
        }

        //
        public bool BandExists(Guid bandId)
        {
            if (bandId == Guid.Empty)
                throw new ArgumentNullException(nameof(bandId));

            return _context.Bands.Any(b => b.Id == bandId);
        }

        public void DeleteAlbum(Album album)
        {
            if (album == null)
                throw new ArgumentNullException(nameof(album));
            _context.Albums.Remove(album);
        }

        public void DeleteBand(Band band)
        {
            if (band == null)
                throw new ArgumentNullException(nameof(band));
            _context.Bands.Remove(band);
        }

        public Album GetAlbum(Guid bandId, Guid albumId)
        {
            if (bandId == Guid.Empty)
                throw new ArgumentNullException(nameof(bandId));
            if (albumId == Guid.Empty)
                throw new ArgumentNullException(nameof(albumId));
            return _context.Albums.Where(a => a.BandId == bandId && a.Id == albumId).FirstOrDefault();
        }

        public IEnumerable<Album> GetAlbums(Guid bandId)
        {
            if (bandId == Guid.Empty)
                throw new ArgumentNullException(nameof(bandId));
            return _context.Albums.Where(a => a.BandId == bandId).OrderBy(a => a.Title).ToList();
        }

        public Band GetBand(Guid bandId)
        {
            if (bandId == Guid.Empty)
                throw new ArgumentNullException(nameof(bandId));
            return _context.Bands.FirstOrDefault(b => b.Id == bandId);
        }

        public IEnumerable<Band> GetBands()
        {
            return _context.Bands.ToList();
        }

        public IEnumerable<Band> GetBands(IEnumerable<Guid> bandIds)
        {
            if (bandIds == null)
                throw new ArgumentNullException(nameof(bandIds));
            return _context.Bands.Where(b => bandIds.Contains(b.Id)).OrderBy(b => b.Name).ToList();
        }

        public PagedList<Band> GetBands(BandsResourceParameters bandsResourceParameters)
        {
            if (bandsResourceParameters == null)
                throw new ArgumentNullException(nameof(bandsResourceParameters));

            var collection = _context.Bands as IQueryable<Band>;

            if (!string.IsNullOrWhiteSpace(bandsResourceParameters.MainGenre))
            {
                var mainGenre = bandsResourceParameters.MainGenre.Trim();
                collection = collection.Where(b => b.MainGener == mainGenre);
            }
            if (!string.IsNullOrWhiteSpace(bandsResourceParameters.SearchQuery))
            {
                var searchQuery = bandsResourceParameters.SearchQuery.Trim();
                collection = collection.Where(b => b.Name.Contains(searchQuery));
            }
            if (!string.IsNullOrWhiteSpace(bandsResourceParameters.OrderBy))
            {
                var searchQuery = bandsResourceParameters.SearchQuery.Trim();
                collection = collection.Where(b => b.Name.Contains(searchQuery));
            }


            return PagedList<Band>.Create(collection, bandsResourceParameters.PageNumber, bandsResourceParameters.PageSize);

        }

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }

        public void UpdateAlbum(Album album)
        {
            //not implemented
        }

        //public void UpdateBand(Band band)
        //{
        //    //not implemented
        //}
    }
}
