﻿using AutoMapper;
using Inmergers.Common.Until;
using Inmergers.Common.Until.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inmergers.Business.Services
{
    public class BandModel : Profile
    {
        /* public Guid Id { get; set; }
         public string Title { get; set; }
         public string UnaccentTitle { get; set; }
         public string Slug { get; set; }
         public string Summary { get; set; }
         public string Content { get; set; }

         public string Thumbnail { get; set; }
         public bool IsPinned { get; set; }

         public DateTime? CreatedOnDate { get; set; }*/

        public Guid Id { get; set; }
        public string Name { get; set; }
        public string FoundedYearsAgo { get; set; }
        public string MainGener { get; set; }
    }

    public class BandGetId
    {

        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Founded { get; set; }
        public string MainGener { get; set; }
    }

    public class BandCreateModel
    {
        public string Name { get; set; }
        public DateTime Founded { get; set; }
        public string MainGener { get; set; }
    }

    public class BandUpdateModel
    {
        public string Name { get; set; }
        public DateTime Founded { get; set; }
        public string MainGener { get; set; }
    }

    public class PostCreateModel
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }
        public string Thumbnail { get; set; }
        public bool IsPinned { get; set; }
        public Guid CategoryId { get; set; }
    }

    public class PostUpdateModel
    {
        public string Title { get; set; }
        public string Summary { get; set; }
        public string Content { get; set; }
        public string Thumbnail { get; set; }
        public bool IsPinned { get; set; }
    }

    public class BandQueryModel : PaginationRequest
    {
        public Guid? CategoryId { get; set; }
    }
}

