﻿using BandAPI.Entities;
using BandAPI.Models;
using Inmergers.Business.Services;
using Inmergers.Common.Until;
using Inmergers.Common.Until.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BandAPI.Business.Services
{
    public interface IBandAlbumRepository
    {
        Task<Response> Get(BandQueryModel filter);
        Task<Response> GetById(Guid BandId);

        Task<Response> Create(BandCreateModel filter);
        Task<Response> Update(Guid id, BandUpdateModel model);
     
        IEnumerable<Album> GetAlbums(Guid bandId);
        Album GetAlbum(Guid bandId, Guid albumId);
        void AddAlbum(Guid bandId, Album album);
        void UpdateAlbum(Album album);
        void DeleteAlbum(Album album);

        IEnumerable<Band> GetBands();
        Band GetBand(Guid bandId);
        IEnumerable<Band> GetBands(IEnumerable<Guid> bandIds);
        PagedList<Band> GetBands(BandsResourceParameters bandsResourceParameters);
        void AddBand(Band band);
        //void UpdateBand(Band band);
        /*void DeleteBand(Band band);*/

        Task<Response> DeleteBand(Guid bandId);
        bool BandExists(Guid bandId);
        bool AlbumExists(Guid albumId);
        bool Save();
    }
}
