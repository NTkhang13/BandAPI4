﻿namespace Inmergers.Common.Until.Helpers
{
    public enum UriType
    {
        PreviousPage,
        NextPage
    }
}
