﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Inmergers.Common.Until
{
    public class Response
    {
        [JsonConstructor]
        public Response(HttpStatusCode code, string messeger)
        {
            Code = code;
            Messeger = messeger;
        }
        public Response(string messeger)
        {
            Messeger = messeger;
        }
        public Response()
        {

        }
        public HttpStatusCode Code { get; set; }
            = HttpStatusCode.OK;

        public string Messeger { get; set; }
            = "ĐÃ THÀNH CÔNG";

        public long TotalTime { get; set; }
            = 0;
    }

    public class Response<T> : Response
    {
        [JsonConstructor]
        public Response(T data)
        {
            Data = data;
            Code = HttpStatusCode.OK;
        }
        public Response(T data, HttpStatusCode code)
        {
            Data = data;
            Messeger = "OK";
        }
        public Response(HttpStatusCode code, T data, string messeger)
        {
            Code = code;
            Data = data;
            Messeger = messeger;
        }
        public Response()
        {

        }
        // Du lieu tra ve
        public T Data { get; set; }
    }

    public class ResponseList<T> : Response
    {
        [JsonConstructor]
        ///   <summary>
        ///     Trả về dạng mảng
        /// </summary>
        public ResponseList(List<T> data)
        {
            Data = data;
        }

        public ResponseList()
        {
        }
        public List<T> Data { get; set; }
    }
    /// <summary>
    ///     Danh sách dữ liệu trả về
    /// </summary>
    public class ResponsePagination<T> : Response
    {
        [JsonConstructor]
        public ResponsePagination(Pagination<T> data)
        {
            Data = data;
        }


        public Pagination<T> Data { get; set; }
    }
    public class Pagination<T>
    {
        public Pagination()
        {
            Size = 20;
            Page = 1;
            Content = new List<T>();
        }

        /// <summary>
        ///Vị trí trang hiện tại
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        ///Tổng số trang trên toàn hệ thống
        /// </summary>
        public int TotalPages { get; set; }

        /// <summary>
        ///Số bản ghi trên mỗi trang
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        ///Số bản ghi về
        /// </summary>
        public int NumberOfElements { get; set; }

        /// <summary>
        ///Tổng số bản ghi có thể tìm kiếm
        /// </summary>
        public int TotalElements { get; set; }

        /// <summary>
        ///Danh sách dữ liệu trả về
        /// </summary>
        public List<T> Content { get; set; }
    }
    /// <summary>
    ///     Trả về Lỗi
    /// </summary>
    public class ResponseError : Response
    {
        [JsonConstructor]
        public ResponseError(HttpStatusCode code, string message, List<Dictionary<string, string>> errorDetail = null) : base(
            code,
            message)
        {
            ErrorDetail = errorDetail;
        }

        public List<Dictionary<string, string>> ErrorDetail { get; set; }
    }

    /// <summary>
    ///     Trả về dạng đối tượng
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseObject<T> : Response
    {
        [JsonConstructor]
        public ResponseObject(T data)
        {
            Data = data;
        }

        public ResponseObject(T data, string messeger)
        {
            Data = data;
            Messeger = messeger;
        }

        public ResponseObject(T data, string messeger, HttpStatusCode code)
        {
            Code = code;
            Data = data;
            Messeger = messeger;
        }

        /// <summary>
        ///     Dữ liệu trả về
        /// </summary>
        public T Data { get; set; }
    }

        /// <summary>
    ///     Trả về kết quả cập nhật dữ liệu
    /// </summary>
    public class ResponseUpdate : Response
    {
        [JsonConstructor]
        public ResponseUpdate(Guid bandId)
        {
            Data = new ResponseUpdateModel { BandId = bandId };
        }
        public ResponseUpdate(Guid bandId, string message) : base(message)
        {
            Data = new ResponseUpdateModel { BandId = bandId };
        }

        public ResponseUpdate(HttpStatusCode code, string message, Guid BandId) : base(code, message)
        {
            Data = new ResponseUpdateModel { BandId = BandId };
        }
        public ResponseUpdate() { }
        public ResponseUpdateModel Data { get; set; }
    }
    /// <summary>
    ///     Đối tượng kết quả cập nhật
    /// </summary>
    public class ResponseUpdateModel
    {
        public Guid BandId { get; set; }
    }
    /// <summary>
    ///     Trả về kết quả cập nhật nhiều dữ liệu
    /// </summary>
    public class ResponseUpdateMulti : Response
    {
        [JsonConstructor]
        public ResponseUpdateMulti(List<ResponseUpdate> data)
        {
            Data = data;
        }

        public ResponseUpdateMulti()
        {
        }

        /// <summary>
        ///     Danh sách dữ liệu trả về
        /// </summary>
        public List<ResponseUpdate> Data { get; set; }
    }
    /*public class ResponseDelete : Response
    {
        [JsonConstructor]
        public ResponseDelete(Guid id, string name)
        {
            Data = new ResponseDeleteModel { Id = id, Name = name };
        }

        public ResponseDelete(HttpStatusCode code, string message, Guid id, string name) : base(code, message)
        {
            Data = new ResponseDeleteModel { Id = id, Name = name };
        }

        public ResponseDelete()
        {
        }

        /// <summary>
        ///     Danh sách dữ liệu trả về
        /// </summary>
        public ResponseDeleteModel Data { get; set; }
    }
    public class ResponseDeleteModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }*/
}
