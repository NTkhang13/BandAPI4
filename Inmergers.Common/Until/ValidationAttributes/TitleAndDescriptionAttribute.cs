﻿using BandAPI.Models;
using System.ComponentModel.DataAnnotations;

namespace BandApi.Until.Common.ValidationAttributes
{
	public class TitleAndDescriptionAttribute : ValidationAttribute
	{
		protected override ValidationResult IsValid(object value, ValidationContext validationContext)
		{
			var album = (AlbumManipulationDto)validationContext.ObjectInstance;

			if (album.Title == album.Description)
			{
				return new ValidationResult("The title and despcription need to be diffrent", new[] { "AlbumManipulationDto" });
			}
			return ValidationResult.Success;
		}
	}
}
