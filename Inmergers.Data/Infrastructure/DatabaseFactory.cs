﻿using AutoMapper.Configuration;
using BandAPI.DbContexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Inmergers.Data
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private readonly DbContext _dataContext;
        private readonly IConfiguration _configuration;

        public DatabaseFactory()
        {
            _dataContext = new BandAlbumContext.DbContext();
        }

        public DbContext GetDbContext()
        {
            return _dataContext;
        }
    }
}