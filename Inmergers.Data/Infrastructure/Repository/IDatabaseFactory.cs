using Microsoft.EntityFrameworkCore;

namespace Inmergers.Data
{
    public interface IDatabaseFactory
    {
        DbContext GetDbContext();
    }
}